﻿using NugetPackageLib.Models;
using NugetPackageLib.Validation;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NugetPackageLib.Services
{
    public class ElasticAgent
    {
        private Globals globals;
        
        public async Task<ResultItem<CreatePackageConfigRaw>> GetCreatePackageConfig(CreatePackageRequest request, bool doValidation = true)
        {
            var taskResult = await Task.Run<ResultItem<CreatePackageConfigRaw>>(() =>
           {
               var result = new ResultItem<CreatePackageConfigRaw>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new CreatePackageConfigRaw()
               };

               if (doValidation)
               {
                   result.ResultState = ValidationController.ValidateCreatePackageRequest(request, globals);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       return result;
                   }
               }
               
               var searchRootConfig = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = request.IdConfig,
                       GUID_Parent = Config.LocalData.Class_NugetPackageLib.GUID
                   }
               };

               var dbReaderRootConfig = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderRootConfig.GetDataObjects(searchRootConfig);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Root-config!";
                   return result;
               }

               result.Result.RootConfig = dbReaderRootConfig.Objects1.FirstOrDefault();

               if (doValidation)
               {
                   result.ResultState = ValidationController.ValidateCreatePackageConfigRaw(result.Result, globals, nameof(result.Result.RootConfig));

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       return result;
                   }
               }
                   
               var searchSubConfigs = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Object = result.Result.RootConfig.GUID,
                       ID_RelationType = Config.LocalData.ClassRel_NugetPackageLib_contains_NugetPackageLib.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_NugetPackageLib_contains_NugetPackageLib.ID_Class_Right
                   }
               };

               var dbReaderSubConfigs = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderSubConfigs.GetDataObjectRel(searchSubConfigs);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the Sub-Configs!";
                   return result;
               }

               result.Result.Configs = dbReaderSubConfigs.ObjectRels.Select(rel => new clsOntologyItem
               {
                   GUID = rel.ID_Other,
                   Name = rel.Name_Other,
                   GUID_Parent = rel.ID_Parent_Other,
                   Type = rel.Ontology
               }).ToList();


               if (!result.Result.Configs.Any())
               {
                   result.Result.Configs.Add(result.Result.RootConfig);
               }

               var searchPackageConfigsCreate = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_NugetPackageLib_Create_Package_Config__Nuget_.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_NugetPackageLib_Create_Package_Config__Nuget_.ID_Class_Right
               }).ToList();

               var dbReaderPackageConfigsCreate = new OntologyModDBConnector(globals);

               if (searchPackageConfigsCreate.Any())
               {
                   result.ResultState = dbReaderPackageConfigsCreate.GetDataObjectRel(searchPackageConfigsCreate);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Packag-Config for creation!";
                   }

                   result.Result.ConfigsToPackageConfigsCreate = dbReaderPackageConfigsCreate.ObjectRels;
               }
               

               if (doValidation)
               {
                   result.ResultState = ValidationController.ValidateCreatePackageConfigRaw(result.Result, globals, nameof(result.Result.ConfigsToPackageConfigsCreate));

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       return result;
                   }
               }
                   

               var searchPackageConfigsNeeds = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_NugetPackageLib_needs_Package_Config__Nuget_.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_NugetPackageLib_needs_Package_Config__Nuget_.ID_Class_Right
               }).ToList();

               var dbReaderPackageConfigsNeeds = new OntologyModDBConnector(globals);

               if (searchPackageConfigsNeeds.Any())
               {
                   result.ResultState = dbReaderPackageConfigsNeeds.GetDataObjectRel(searchPackageConfigsNeeds);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the needed Packag-Config!";
                   }

                   result.Result.ConfigsToPackageConfigsNeeds = dbReaderPackageConfigsNeeds.ObjectRels;
               }
               
               if (doValidation)
               {
                   result.ResultState = ValidationController.ValidateCreatePackageConfigRaw(result.Result, globals, nameof(result.Result.ConfigsToPackageConfigsNeeds));

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       return result;
                   }
               }

               var searchDescriptions = result.Result.ConfigsToPackageConfigsCreate.Select(rel => new clsObjectAtt
               {
                   ID_Object = rel.ID_Other,
                   ID_AttributeType = Config.LocalData.AttributeType_Description.GUID
               }).ToList();

               searchDescriptions.AddRange(result.Result.ConfigsToPackageConfigsNeeds.Select(rel => new clsObjectAtt
               {
                   ID_Object = rel.ID_Other,
                   ID_AttributeType = Config.LocalData.AttributeType_Description.GUID
               }));

               var dbReaderDescription = new OntologyModDBConnector(globals);

               if (searchDescriptions.Any())
               {
                   result.ResultState = dbReaderDescription.GetDataObjectAtt(searchDescriptions);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the descriptions!";
                       return result;
                   }

                   result.Result.PackageConfigsToDescriptions = dbReaderDescription.ObjAtts;
               }
               
               if (doValidation)
               {
                   result.ResultState = ValidationController.ValidateCreatePackageConfigRaw(result.Result, globals, nameof(result.Result.PackageConfigsToDescriptions));

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       return result;
                   }
               }

               var searchAuthors = result.Result.ConfigsToPackageConfigsCreate.Select(rel => new clsObjectRel
               {
                   ID_Object = rel.ID_Other,
                   ID_RelationType = Config.LocalData.ClassRel_Package_Config__Nuget__Autor_Partner.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Package_Config__Nuget__Autor_Partner.ID_Class_Right
               }).ToList();

               var dbReaderAuthors = new OntologyModDBConnector(globals);

               if (searchAuthors.Any())
               {
                   result.ResultState = dbReaderAuthors.GetDataObjectRel(searchAuthors);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Authors!";
                       return result;
                   }

                   result.Result.PackageConfigsToAuthor = dbReaderAuthors.ObjectRels;
               }
               
               if (doValidation)
               {
                   result.ResultState = ValidationController.ValidateCreatePackageConfigRaw(result.Result, globals, nameof(result.Result.PackageConfigsToAuthor));

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       return result;
                   }
               }

               var searchPacketToPacketConfiguration = result.Result.ConfigsToPackageConfigsCreate.Select(rel => new clsObjectRel
               {
                   ID_Other = rel.ID_Other,
                   ID_RelationType = Config.LocalData.ClassRel_Package__Nuget__is_defined_by_Package_Config__Nuget_.ID_RelationType,
                   ID_Parent_Object = Config.LocalData.ClassRel_Package__Nuget__is_defined_by_Package_Config__Nuget_.ID_Class_Left
               }).ToList();

               var dbReaderPacketToPacketConfiguration = new OntologyModDBConnector(globals);

               if (searchPacketToPacketConfiguration.Any())
               {
                   result.ResultState = dbReaderPacketToPacketConfiguration.GetDataObjectRel(searchPacketToPacketConfiguration);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Nuget Packets!";
                       return result;
                   }

                   result.Result.PackagesToPackageConfigs = dbReaderPacketToPacketConfiguration.ObjectRels;
               }

               if (doValidation)
               {
                   result.ResultState = ValidationController.ValidateCreatePackageConfigRaw(result.Result, globals, nameof(result.Result.PackagesToPackageConfigs));

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       return result;
                   }
               }

               var searchDevelopmentVersions = result.Result.ConfigsToPackageConfigsCreate.Select(rel => new clsObjectRel
               {
                   ID_Object = rel.ID_Other,
                   ID_RelationType = Config.LocalData.ClassRel_Package_Config__Nuget__is_defined_by_Development_Version.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Package_Config__Nuget__is_defined_by_Development_Version.ID_Class_Right
               }).ToList();

               var dbReaderDevelopmentVersions = new OntologyModDBConnector(globals);

               if (searchDevelopmentVersions.Any())
               {
                   result.ResultState = dbReaderDevelopmentVersions.GetDataObjectRel(searchDevelopmentVersions);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Development-Versions!";
                       return result;
                   }

                   result.Result.PackageConfigsToDevelopmentVersions = dbReaderDevelopmentVersions.ObjectRels;
               }

               if (doValidation)
               {
                   result.ResultState = ValidationController.ValidateCreatePackageConfigRaw(result.Result, globals, nameof(result.Result.PackageConfigsToDevelopmentVersions));

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       return result;
                   }
               }

               var searchPaths = result.Result.Configs.Select(config => new clsObjectRel
               {
                   ID_Object = config.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_NugetPackageLib_belonging_Source_Path.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_NugetPackageLib_belonging_Source_Path.ID_Class_Right
               }).ToList();

               var dbReaderGetPaths = new OntologyModDBConnector(globals);

               if (searchPaths.Any())
               {
                   result.ResultState = dbReaderGetPaths.GetDataObjectRel(searchPaths);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Paths!";
                       return result;
                   }

                   result.Result.PackageConfigsToPaths = dbReaderGetPaths.ObjectRels;
               }
               

               return result;
           });

            return taskResult;
        }

        public ElasticAgent(Globals globals)
        {
            this.globals = globals;
        }
    }
}
