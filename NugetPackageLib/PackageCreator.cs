﻿using MediaStore_Module;
using NugetPackageLib.Models;
using NugetPackageLib.Services;
using NugetPackageLib.Validation;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace NugetPackageLib
{
    public class PackageCreator : AppController
    {

        private package package = new package();
        private List<packageFile> files = new List<packageFile>();
        private List<dependency> dependencies = new List<dependency>();

        public packageMetadata Metadata
        {
            get
            {
                if (package.metadata == null)
                {
                    package.metadata = new packageMetadata();
                }
                return package.metadata;
            }
        }

        public ResultAddFiles AddFiles(string srcPath, string rootSrcPath, string rootDstPath)
        {
            var result = new ResultAddFiles
            {
                IsAdded = true
            };

            var filePaths = Directory.GetFiles(srcPath);
            foreach (var filePath in filePaths)
            {

                var relative = Path.GetDirectoryName(filePath);
                relative = relative.Replace(rootSrcPath, "");
                var dstPath = rootDstPath + relative;
                var resultFile = AddFile(filePath, dstPath);
                if (result.IsAdded == false)
                {
                    result.IsAdded = false;
                    result.FilesWithError.AddRange(resultFile.FilesWithError);
                }
            }

            var directoryPaths = Directory.GetDirectories(srcPath);
            foreach (var directoryPath in directoryPaths)
            {
                AddFiles(directoryPath, rootSrcPath, rootDstPath);
            }

            return result;
        }

        

        public ResultAddFiles AddFile(string srcPath, string dstPath, string exclude = null)
        {
            var result = new ResultAddFiles
            {
                IsAdded = true
            };

            if (string.IsNullOrEmpty(exclude))
            {
                files.Add(new packageFile
                {
                    src = srcPath,
                    target = dstPath
                });
            }
            else
            {
                files.Add(new packageFile
                {
                    src = srcPath,
                    exclude = exclude
                });
            }

            return result;
        }

        public ResultSavePackageSpec SaveSpec(string specPath)
        {
            var result = new ResultSavePackageSpec
            {
                IsSaved = true
            };

            try
            {
                package.files = files.ToArray();

                if (dependencies.Any())
                {
                    package.metadata.dependencies = new packageMetadataDependencies();
                    package.metadata.dependencies.Items = dependencies.ToArray();
                }
                
                var serializer = new XmlSerializer(typeof(package));

                using (var textWriter = new StreamWriter(specPath))
                {
                    serializer.Serialize(textWriter, package);
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                
            }

            return result;
            
        }

        public ResultAddDependency AddDependency(string packageName, string version)
        {
            var result = new ResultAddDependency
            {
                IsAdded = true
            };

            var dependency = new dependency
            {
                id = packageName,
                version = version
            };

            dependencies.Add(dependency);

            return result;
        }

        public ResultPackNugetPackage PackPackage(string pathNugetExe, string specPath, string outputPath)
        {
            var result = new ResultPackNugetPackage
            {
                IsPacked = true
            };

            var startInfo = new ProcessStartInfo(pathNugetExe, $"pack \"{ specPath }\"");

            startInfo.WorkingDirectory = outputPath;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;
            var process = new Process
            {
                StartInfo = startInfo
                
            };
            try
            {
                process.Start();
                result.Message = process.StandardOutput.ReadToEnd();
                process.WaitForExit();
                result.IsPacked = process.ExitCode == 0;
                
            }
            catch (Exception ex)
            {
                result.IsPacked = false;
                result.Message = ex.Message;
                
                
            }
            

            return result;
        }

        public PackageCreator(Globals globals) : base(globals)
        {
        }
        public async Task<ResultItem<List<CreatePackageResult>>> CreatePackage(CreatePackageRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<CreatePackageResult>>>(async() =>
           {
               var result = new ResultItem<List<CreatePackageResult>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new List<CreatePackageResult>()
               };

               var elasticAgent = new ElasticAgent(Globals);

               request.MessageOutput?.OutputInfo("Validate request...");
               result.ResultState = ValidationController.ValidateCreatePackageRequest(request, Globals);

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Validated request.");


               request.MessageOutput?.OutputInfo("Get Model...");
               var getModel = await elasticAgent.GetCreatePackageConfig(request, false);

               result.ResultState = getModel.ResultState;
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               var validateModel = ValidationController.ValidateCreatePackageConfigRaw(getModel.Result, Globals);

               result.ResultState = validateModel;
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Have Model.");

               var mediaStoreController = new MediaStoreConnector(Globals);

               request.MessageOutput?.OutputInfo("Check Mediastore...");
               var storePresent = await mediaStoreController.CheckMediaService();

               result.ResultState = storePresent;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }
               request.MessageOutput?.OutputInfo("Checked Mediastore.");

               foreach (var config in getModel.Result.Configs)
               {
                   request.MessageOutput?.OutputInfo($"Config {config.Name}");
                   var resultItem = new CreatePackageResult
                   {
                       ResultState = Globals.LState_Success.Clone()
                   };
                   result.Result.Add(resultItem);

                   var configToPackageCreate = getModel.Result.ConfigsToPackageConfigsCreate.First(rel => rel.ID_Object == config.GUID);
                   var configToPackageNeeds = getModel.Result.ConfigsToPackageConfigsNeeds.Where(rel => rel.ID_Object == config.GUID);

                   var version = getModel.Result.PackageConfigsToDevelopmentVersions.Where(rel => rel.ID_Object == configToPackageCreate.ID_Other).Select(rel => rel.Name_Other).First();
                   var packageName = getModel.Result.PackagesToPackageConfigs.Where(rel => rel.ID_Other == configToPackageCreate.ID_Other).Select(rel => rel.Name_Object).First();
                   var author = getModel.Result.PackageConfigsToAuthor.Where(rel => rel.ID_Object == configToPackageCreate.ID_Other).Select(rel => rel.Name_Other).First();
                   var description = getModel.Result.PackageConfigsToDescriptions.Where(rel => rel.ID_Object == configToPackageCreate.ID_Other).Select(rel => rel.Val_String).First();

                   var path = getModel.Result.PackageConfigsToPaths.Where(rel => rel.ID_Object == config.GUID).Select(rel => rel.Name_Other).First();

                   var neededPackages = (from needsRel in configToPackageNeeds
                                         join package in getModel.Result.PackagesToPackageConfigs on needsRel.ID_Other equals package.ID_Other
                                         join versionItem in getModel.Result.PackageConfigsToDevelopmentVersions on needsRel.ID_Other equals versionItem.ID_Object
                                         select new { Package = package.Name_Object, Version = versionItem.Name_Other });

                   var refItem = new clsOntologyItem
                   {
                       GUID = configToPackageCreate.ID_Other,
                       Name = configToPackageCreate.Name_Other,
                       GUID_Parent = configToPackageCreate.ID_Parent_Other,
                       Type = configToPackageCreate.Ontology
                   };

                   request.MessageOutput?.OutputInfo("Check related package...");

                   var filesResult = await mediaStoreController.GetFileItems(refItem);
                   result.ResultState = filesResult.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(resultItem.ResultState.Additional1);
                       continue;
                   }

                   if (filesResult.Result.FileItemsManaged.Any(file => file.Name_Object.ToLower().EndsWith(".nupkg")))
                   {
                       resultItem.ResultState = Globals.LState_Error.Clone();
                       resultItem.ResultState.Additional1 = "An package is related to the package-config!";
                       request.MessageOutput?.OutputError(resultItem.ResultState.Additional1);
                       continue;
                   }

                   foreach (var fileItem in filesResult.Result.FileItemsManaged)
                   {
                       var filePath = System.IO.Path.Combine(path, fileItem.Name_Object);
                       resultItem.ResultState = mediaStoreController.SaveManagedMediaToFile(new clsOntologyItem { GUID = fileItem.ID_Object, Name = fileItem.Name_Object, GUID_Parent = fileItem.ID_Class, Type = Globals.Type_Object }, filePath, true, false);
                       if (result.ResultState.GUID == Globals.LState_Error.GUID)
                       {
                           request.MessageOutput?.OutputError(resultItem.ResultState.Additional1);
                           continue;
                       }
                   }
                   

                   request.MessageOutput?.OutputInfo($"Get files...");
                   try
                   {
                       if (!Directory.Exists(path))
                       {
                           resultItem.ResultState = Globals.LState_Error.Clone();
                           resultItem.ResultState.Additional1 = "Src-Path does not exist!";
                           request.MessageOutput?.OutputError(resultItem.ResultState.Additional1);
                           continue;
                       }
                   }
                   catch (Exception ex)
                   {
                       
                       resultItem.ResultState = Globals.LState_Error.Clone();
                       resultItem.ResultState.Additional1 = ex.Message;
                       request.MessageOutput?.OutputError(resultItem.ResultState.Additional1);
                       continue;
                   }

                   request.MessageOutput?.OutputInfo($"Create Meta...");

                   var attribute = (XmlTypeAttribute)typeof(package).GetCustomAttributes(false).FirstOrDefault(attrib => attrib is XmlTypeAttribute);

                   if (attribute != null)
                   {
                       attribute.Namespace = "https://raw.githubusercontent.com/NuGet/NuGet.Client/dev/src/NuGet.Core/NuGet.Packaging/compiler/resources/nuspec.xsd";
                   }
                   package.metadata = new packageMetadata();
                   package.metadata.version = version;
                   package.metadata.id = packageName;
                   package.metadata.authors = author;
                   package.metadata.description = description;

                   request.MessageOutput?.OutputInfo($"Created Meta: Id: {packageName}, Version: {version}, Author: {author}, Description. {description}");

                   request.MessageOutput?.OutputInfo($"Add Files...");
                   try
                   {
                       var fileItemsResult = AddFiles(path, path, config.Name);
                       if (fileItemsResult.FilesWithError.Any())
                       {
                           resultItem.ResultState = Globals.LState_Error.Clone();
                           resultItem.ResultState.Additional1 = $"{fileItemsResult.FilesWithError.Count} Files with errors!";
                           request.MessageOutput?.OutputError(resultItem.ResultState.Additional1);
                           continue;
                       }

                       if (!fileItemsResult.IsAdded)
                       {
                           resultItem.ResultState = Globals.LState_Error.Clone();
                           resultItem.ResultState.Additional1 = "No files added!";
                           request.MessageOutput?.OutputError(resultItem.ResultState.Additional1);
                           continue;
                       }
                   }
                   catch (Exception ex)
                   {
                       resultItem.ResultState = Globals.LState_Error.Clone();
                       resultItem.ResultState.Additional1 = ex.Message;
                       request.MessageOutput?.OutputError(resultItem.ResultState.Additional1);
                       continue;
                   }

                   request.MessageOutput?.OutputInfo($"Added {files.Count} files.");

                   request.MessageOutput?.OutputInfo($"Add {neededPackages.Count()} dependencies...");
                   foreach (var package in neededPackages)
                   {
                       var neededResult = AddDependency(package.Package, package.Version);
                       if (!neededResult.IsAdded)
                       {
                           resultItem.ResultState.GUID = Globals.LState_Error.GUID;
                           resultItem.ResultState.Additional1 = $"Error while adding the dependency {package.Package} ({package.Version})";
                           break;
                       }
                   }

                   request.MessageOutput?.OutputInfo($"Added dependencies.");

                   if (resultItem.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       continue;
                   }

                   var tempPath = Environment.ExpandEnvironmentVariables($"%TEMP%\\{Guid.NewGuid().ToString()}");
                   try
                   {
                       if (!Directory.Exists(tempPath))
                       {
                           Directory.CreateDirectory(tempPath);
                       }
                   }
                   catch (Exception ex)
                   {
                       resultItem.ResultState = Globals.LState_Error.Clone();
                       resultItem.ResultState.Additional1 = ex.Message;
                       request.MessageOutput?.OutputError(resultItem.ResultState.Additional1);
                   }
                   var specPath = Environment.ExpandEnvironmentVariables($"{tempPath}\\{Guid.NewGuid().ToString()}.nuspec");
                   var specResult = SaveSpec(specPath);
                   if (!specResult.IsSaved)
                   {
                       resultItem.ResultState = Globals.LState_Error.Clone();
                       resultItem.ResultState.Additional1 = specResult.Message;
                       request.MessageOutput?.OutputError(resultItem.ResultState.Additional1);
                       continue;
                   }
                   var pathExe = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "nuget.exe");
                   var packResult = PackPackage(pathExe, specPath, tempPath);

                   if (!packResult.IsPacked)
                   {
                       resultItem.ResultState = Globals.LState_Error.Clone();
                       resultItem.ResultState.Additional1 = packResult.Message;
                       request.MessageOutput?.OutputError(resultItem.ResultState.Additional1);
                       continue;
                   }

                   var packageFiles = Directory.GetFiles(tempPath, "*.nupkg");

                   if (!packageFiles.Any())
                   {
                       resultItem.ResultState = Globals.LState_Error.Clone();
                       resultItem.ResultState.Additional1 = $"Package-file does not exist in path {tempPath}!";
                       request.MessageOutput?.OutputError(resultItem.ResultState.Additional1);
                       continue;
                   }

                   var packagePath = packageFiles[0];

                   var saveResult =  await mediaStoreController.SaveFileToManagedMedia(packagePath);
                   result.ResultState = saveResult.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(resultItem.ResultState.Additional1);
                       continue;
                   }

                   resultItem.FileItem = saveResult.Result;

                   var relResult = await mediaStoreController.RelateFileItemToRef(refItem, saveResult.Result, 1);

                   result.ResultState = relResult.ResultState;

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       request.MessageOutput?.OutputError(resultItem.ResultState.Additional1);
                       continue;
                   }

               }

               return result;
           });

            return taskResult;

            

        }
    }

    public class ResultAddFiles
    {
        public bool IsAdded { get; set; }
        public List<packageFile> FilesWithError { get; set; }

        public ResultAddFiles()
        {
            FilesWithError = new List<packageFile>();
        }
       
    }

    public class ResultSavePackageSpec
    {
        public bool IsSaved { get; set; }
        public string Message { get; set; }
    }

    public class ResultPackNugetPackage
    {
        public bool IsPacked { get; set; }
        public string Message { get; set; }
    }
    
    public class ResultAddDependency
    {
        public bool IsAdded { get; set; }
    }

}
