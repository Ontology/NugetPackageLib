﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NugetPackageLib.Models
{
    public class CreatePackageRequest
    {
        public string IdConfig { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public CreatePackageRequest(string idConfig)
        {
            IdConfig = idConfig;
        }
    }
}
