﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NugetPackageLib.Models
{
    public class CreatePackageResult
    {
        public clsOntologyItem ResultState { get; set; }
        public clsOntologyItem FileItem { get; set; }
    }
}
