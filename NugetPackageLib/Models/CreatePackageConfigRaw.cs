﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NugetPackageLib.Models
{
    public class CreatePackageConfigRaw
    {
        public clsOntologyItem RootConfig { get; set; }
        public List<clsOntologyItem> Configs { get; set; } = new List<clsOntologyItem>();

        public List<clsObjectRel> ConfigsToPackageConfigsCreate { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> ConfigsToPackageConfigsNeeds { get; set; } = new List<clsObjectRel>();

        public List<clsObjectAtt> PackageConfigsToDescriptions { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectRel> PackageConfigsToDevelopmentVersions { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> PackageConfigsToAuthor { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> PackageConfigsToPaths { get; set; } = new List<clsObjectRel>();

        public List<clsObjectRel> PackagesToPackageConfigs { get; set; } = new List<clsObjectRel>();

    }
}
