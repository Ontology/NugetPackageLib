﻿using NugetPackageLib.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NugetPackageLib.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateCreatePackageRequest(CreatePackageRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Config-Id is empty!";
                return result;
            }

            if (!globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "Config-Id is no valid GUID!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateCreatePackageConfigRaw(CreatePackageConfigRaw config, Globals globals, string propertyName = null)
        {
            var result = globals.LState_Success.Clone();

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(config.RootConfig))
            {
                if (config.RootConfig == null)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "No Root-config found!";
                    return result;
                }
            }
            
            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(config.ConfigsToPackageConfigsCreate))
            {
                if (config.ConfigsToPackageConfigsCreate.Count != config.Configs.Count)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"{config.Configs.Count} Configs and {config.ConfigsToPackageConfigsCreate.Count} Package-Configs. Use on Package-Config per Config!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(config.PackageConfigsToDescriptions))
            {
                var necessaryDescriptions = (from createConfig in config.ConfigsToPackageConfigsCreate
                                             join desc in config.PackageConfigsToDescriptions on createConfig.ID_Other equals desc.ID_Object into descs
                                             from desc in descs.DefaultIfEmpty()
                                             select new { createConfig, desc }).ToList();

                var countWrong = necessaryDescriptions.Count(desc => desc.desc == null);
                if (countWrong > 0)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"{countWrong} Configs have no descriptions!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(config.PackageConfigsToAuthor))
            {
                var necessaryAuthors = (from createConfig in config.ConfigsToPackageConfigsCreate
                                             join author in config.PackageConfigsToAuthor on createConfig.ID_Other equals author.ID_Object into authors
                                             from author in authors.DefaultIfEmpty()
                                             select new { createConfig, author }).ToList();

                var countWrong = necessaryAuthors.Count(author => author.author == null);
                if (countWrong > 0)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"{countWrong} Configs have no author!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(config.PackagesToPackageConfigs))
            {
                var necessaryPackets = (from createConfig in config.ConfigsToPackageConfigsCreate
                                        join packet in config.PackagesToPackageConfigs on createConfig.ID_Other equals packet.ID_Other into packets
                                        from packet in packets.DefaultIfEmpty()
                                        select new { createConfig, packet }).ToList();

                var countWrong = necessaryPackets.Count(author => author.packet == null);
                if (countWrong > 0)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"{countWrong} Configs have no packets!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(config.PackageConfigsToDevelopmentVersions))
            {
                var necessaryVersions = (from createConfig in config.ConfigsToPackageConfigsCreate
                                        join version in config.PackageConfigsToDevelopmentVersions on createConfig.ID_Other equals version.ID_Object into versions
                                         from version in versions.DefaultIfEmpty()
                                        select new { createConfig, version }).ToList();

                var countWrong = necessaryVersions.Count(author => author.version == null);
                if (countWrong > 0)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"{countWrong} Configs have no version!";
                    return result;
                }
            }

            if (string.IsNullOrEmpty(propertyName) || propertyName == nameof(config.PackageConfigsToPaths))
            {
                
                if (config.Configs.Count != config.PackageConfigsToPaths.Count)
                {
                    var configsWithoutPath = config.Configs.Count - config.PackageConfigsToPaths.Count;
                    result = globals.LState_Error.Clone();
                    if (configsWithoutPath > 0)
                    {
                        result.Additional1 = $"{configsWithoutPath} Configs have no Path!";
                    }
                    else
                    {
                        result.Additional1 = $"{configsWithoutPath} Paths too much!";
                    }
                    
                    return result;
                }
            }

            if (propertyName == null)
            {
                var necessaryVersions = (from needConfig in config.ConfigsToPackageConfigsNeeds
                                         join version in config.PackageConfigsToDevelopmentVersions on needConfig.ID_Other equals version.ID_Object into versions
                                         from version in versions.DefaultIfEmpty()
                                         select new { needConfig, version }).ToList();

                var countWrong = necessaryVersions.Count(author => author.version == null);
                if (countWrong > 0)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"{countWrong} Needs have no version!";
                    return result;
                }
            }

            return result;
        }
    }
}
