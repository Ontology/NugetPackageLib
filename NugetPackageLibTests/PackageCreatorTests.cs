﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NugetPackageLib;
using System.Collections.Generic;
using System.IO;

namespace NugetPackageLibTests
{
    [TestClass]
    public class PackageCreatorTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            var packageCreator = new PackageCreator("0.5.3.211", "OntologyModule", "Tassilo Koller", "Ontology-Editor of OModules");
            packageCreator.AddDependency("FileSystemModule", "0.3.2.11");
            packageCreator.AddFiles(@"D:\Gitlab\OntologyModules\OntologyModule\Ontolog-Module\bin\Release", @"D:\Gitlab\OntologyModules\OntologyModule\Ontolog-Module\bin\Release", "OntologyModule");
            packageCreator.SaveSpec(@"C:\Temp\OntologyModule.nuspec");
            packageCreator.PackPackage("nuget.exe", @"C:\Temp\OntologyModule.nuspec");
        }

        
    }
}
